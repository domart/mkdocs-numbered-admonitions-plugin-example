---
title: Chapitre I - Les admonitions
---


# Chapitre I - Les admonitions



## Lien

- Dans le théorème @pythagore, on parlera de Pythagore.

- Dans le théorème @thales, on parlera de [Thalès](https://fr.wikipedia.org/wiki/Thal%C3%A8s){target=_blank}.

- Dans le propriété @somme_arithmetique, on parlera de la somme des termes d'une suite arithmétique.

- Lien vers une propriété des suites géométriques, dans une [autre page](section 1/partie1#geo).

## Les remarques

???+note

    - Les `note` ne sont pas numérotées, car elle ne sont pas référencées dans le `mkdocs.yml` :

        ```yaml title="mkdocs.yml"
        plugins:
            - ...
            - numbered-admonitions:
                admonition_types_to_number:
                    'theoreme': 'Théorème'
                    'definition': 'Définition'
                    'exemple': 'Exemple'
                    'exemples': 'Exemples'
                    'remarque': 'Remarque'
                    'remarques': 'Remarques'
                    'propriete': 'Propriété'
                ...
        ```

        À cet endroit, on indique également par quel libellé le type d'admonition doit être remplacé.

    - Les `remarque` et les `remarques` partagent le même compteur (paramètre `shared_counter` du plugin, dans `mkdocs.yml`) :

        ```yaml title="mkdocs.yml"
        plugins:
            - ...
            - numbered-admonitions:
                admonition_types_to_number:
                    ...
                    'remarque': 'Remarque'
                    'remarques': 'Remarques'
                    ...
                shared_counter:
                    - ...
                    - remarque, remarques
        ```

### Repliable ouverte

???+remarque
    `remarque` au singulier repliable ouverte


### Repliable fermée

???remarques

    `remarques` au pluriel. La numérotation de `remarque` au singulier continue.

    1. Repliable
    1. fermée

### Non repliable

!!!remarques
    `remarques` au pluriel, non repliable.

    1. Non
    1. repliable


Dans ce chapitre, $\mathbb{N}$ désigne l'ensemble des entiers naturels.

## Les autres

### Les propriétés

#### Sans titre

???+propriete
    Soit $(u_n)$ une suite géométrique de raison $q\in\mathbb{R}$.

    Alors, pour tout $n\in\mathbb{N}, n\times{}m$,
    
    $$u_n = u_0\times{}q^n$$

#### Avec un titre

???+propriete "avec titre"
    Soit $(u_n)$ une suite géométrique de raison $q\in\mathbb{R}$.

    Alors, pour tout $n\in\mathbb{N}$,
    
    $$u_n = u_0\times{}q^n$$

### Les théorèmes

#### De Pythagore

???+theoreme "Pythagore" tag="pythagore"
    Le triangle $ABC$ est rectangle en $C$ si, et seulement si, $AB^2=AC^2+BC^2$.

    <center>
        <iframe scrolling="no" title="Pythagore" src="https://www.geogebra.org/material/iframe/id/pxwzce3g/width/350/height/250/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="350px" height="250px" style="border:0px;"> </iframe>
    </center>

#### De Thalès

!!!theoreme "Thalès" tag="thales"
    Soit $ABC$ un triangle. Soient $D$ et $E$ deux points tels que $D\in(AB)$ et $E\in(AC)$.

    Si $(DE)//(BC)$, alors

    $$\dfrac{AD}{AB}=\dfrac{AE}{AC}$$

    <center>
        <iframe scrolling="no" title="Thalès" src="https://www.geogebra.org/material/iframe/id/q5abzm4t/width/700/height/500/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="700px" height="500px" style="border:0px;"> </iframe>
    </center>


## Les suites

### Les suites arithémtiques

!!!definition "Suite arithmétique"
    On dit que $(u_n)_{n\in\mathbb{N}}$ est **arithmétique** si

    $$\exists{}r\in\mathbb{N}, \forall{}n\in\mathbb{N}, u_{n+1}=u_{n}+r$$

    $r$ est appelée la **raison** de la suite.

    
!!!theoreme
    Soit $r\in\mathbb{R}$.

    $(u_n)_{n\in\mathbb{N}}$ est arithmétique de raison $r$ si, et seulement si,

    $$\forall{}n\in\mathbb{N}, \boxed{u_n=u_0+n\times{}r}$$

???+propriete tag="suite_arithmetique"
    Soit $(u_n)_{n\in\mathbb{N}}$ une suite arithmétique de raison $r\in\mathbb{R}$.

    Alors, pour tout $n\in\mathbb{N}$ et pour tout $p\in\mathbb{R}$,

    $$u_n=u_p+\left(n-p\right)\times{}r$$

!!!propriete titre="Avec titre et tag" tag="somme_arithmetique"
    Soit $(u_n)_{n\in\mathbb{N}}$ une suite arithmétique de raison $r\in\mathbb{R}$.

    Alors, pour tout $n\in\mathbb{N}$,

    \[
    \begin{array}{rcl}
        \sum_{k=0}^{n}{u_k} & = & u_0\times\left(n+1\right)+r\dfrac{n(n+1)}{2} \\\\
                            & = & (n+1)\left(\dfrac{2u_0+r\times{}n}{2}\right)
    \end{array}
    \]
    

```mermaid
graph LR
    A(($\Omega$)) -- $1$ --> B((Circle))
    A --> C(Round Rect)
    B --> D[[Rhombus]]
    C --> D
    D --> E{accolade simple}
    D --> F{{Doubles}}
    F --> G[Crochet]
```